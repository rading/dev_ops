import { MigrationInterface, QueryRunner } from 'typeorm';

export class SeedNotesTable1585245137962 implements MigrationInterface {
  name = 'SeedNotesTable1585245137962';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`INSERT INTO "notes" ("text") VALUES ('Hello, world'), ('Devops note'), ('Third record')`, undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DELETE FROM "notes"`, undefined);
  }

}
