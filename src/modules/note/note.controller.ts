import { Controller } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { Crud, CrudController, CrudRequest, Override, ParsedBody, ParsedRequest } from '@nestjsx/crud';
import { NoteEntity } from '@app/modules/note/note.entity';
import { NoteService } from '@app/modules/note/note.service';

@Crud({
  model: {
    type: NoteEntity,
  },
  params: {
    id: { primary: true, field: 'id', type: 'number' },
  },
  routes: {
    only: ['getManyBase', 'createOneBase', 'replaceOneBase', 'deleteOneBase'],
  },
  query: {
    alwaysPaginate: true,
  },
})
@Controller('notes')
@ApiTags('notes')
export class NoteController implements CrudController<NoteEntity> {
  constructor(public readonly service: NoteService) {}

  get base(): CrudController<NoteEntity> {
    return this;
  }

  @Override('createOneBase')
  @ApiOperation({ summary: 'Add note' })
  async addNote(
    @ParsedRequest() req: CrudRequest,
    @ParsedBody() dto: NoteEntity,
  ) {
    return this.base.createOneBase(req, dto);
  }

  @Override('replaceOneBase')
  @ApiOperation({ summary: 'Refresh note' })
  async updateNote(
    @ParsedRequest() req: CrudRequest,
    @ParsedBody() dto: NoteEntity,
  ) {
    return this.base.replaceOneBase(req, dto);
  }

  @Override('deleteOneBase')
  @ApiOperation({ summary: 'Delete note' })
  async deleteNote(
    @ParsedRequest() req: CrudRequest,
  ) {
    return this.base.deleteOneBase(req);
  }

}
