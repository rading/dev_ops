import { Controller, Get, HttpCode, HttpStatus } from '@nestjs/common';
import { ApiNoContentResponse, ApiTags } from '@nestjs/swagger';

@Controller('healthz')
@ApiTags('health')
export class HealthController {
  @Get()
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiNoContentResponse({
    description: 'Returns HTTP 204 when api service is alive',
  })
  public async checkHealth(): Promise<void> {}
}
